const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const sauce = path.resolve(__dirname, "src")

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.bundle.js'
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [sauce],
        exclude: /node_modules/,
        loader: 'webpack-atomizer-loader',
        query: {
          configPath: path.resolve('./atomic.config.js')
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
        options: {
          minimize: true,
          sourceMap: true
        }
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
        options: {
          limit: 10000
        }
      },
      {
        test: /\.js$/,
        include: [sauce],
        exclude: /node_modules/,
        loader: "eslint-loader"
      },
      {
        test: /\.js$/,
        include: [sauce],
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: ['env', 'stage-3', 'react']
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      template: 'public/index.ejs',
    })
  ]
}
